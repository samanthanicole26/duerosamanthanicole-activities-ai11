import React from 'react';
import { StyleSheet, Text, Image, View} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
 
const slides = [
  {
    key: '1',
    title: 'Welcome',
    subtitle: 'Hello, I am Samantha! And heres the\nreason why I chose Information Technology',
    image: require('../images/image1.png'),
  },
  {
    key: '2',
    title: 'Page 1',
    subtitle: 'The IT industry has revolved and improved over the past years, sharing of information became much easier and faster.',
    image: require('../images/image2.png'),
  },
  {
    key: '3',
    title: 'Page 2',
    subtitle: 'During this time, being computer literate is an advantage in the workplace. It gives us a bigger opportunity to apply jobs easily.',
    image: require('../images/image3.png'),
  },
  {
    key: '4',
    title: 'Page 3',
    subtitle: 'Choosing this path will help me improve the skills I have in this area.',
    image: require('../images/image4.png'),
  },
  {
    key: '5',
    title: 'Page 4',
    subtitle: 'Lastly, engaging in this course will teach me more about managing and utilizing the computer and software effectively.',
    image: require('../images/image5.png'),
  },
];

const Slide = ({ item }) => {
    return (
      <View style={styles.slide}>
        <Text style={styles.title}>{item.title}</Text>
        <Image 
            source={item.image}
            style={{height: '75%', width: '100%', resizeMode: 'contain'}}
        />
        <Text style={styles.subtitle}>{item.subtitle}</Text>
      </View>
    );
};

export default function AppIntro ({navigation}) {
    return (
        <AppIntroSlider
          data={slides}
          renderItem={({item}) => <Slide item={item}/>}
          onDone={()=> navigation.push('App Intro')} 

          activeDotStyle={{
            backgroundColor:"#21465b",
            width:10
          }}
          showDoneButton={true}
          renderDoneButton={()=><Text style={{color: 'green', margin: 20, fontSize: 18, fontWeight: 'bold'}}>Home</Text>}
          showNextButton={true}
          renderNextButton={()=><Text style={{color: 'green', margin: 20, fontSize: 18, fontWeight: 'bold'}}>Continue</Text>}
        />
    );
}

const styles = StyleSheet.create({
    slide: {
        alignItems: 'center',
        backgroundColor: 'beige',
        marginTop: 50,
    },
    subtitle: {
        color: 'green',
        fontSize: 18,
        marginTop: 10,
        maxWidth: '75%',
        textAlign: 'center',
        lineHeight: 23,
    },
    title: {
        color: 'green',
        fontSize: 22,
        fontWeight: 'bold',
        marginTop: 10,
        textAlign: 'center',
    },
});