import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Platform,
  ScrollView,
} from "react-native";
import { useState, useEffect, useRef } from "react";

// react native have no exact date time picker so we try to install other libraries
import DateTimePicker from "@react-native-community/datetimepicker";
// to store application data locally
import AsyncStorage from "@react-native-async-storage/async-storage";
// library for push notification
import * as Notifications from "expo-notifications";
//identify if the device is emulator or real device
import * as Device from "expo-device";

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

export default function App() {
  // get the current date
  const today = new Date();
  // there are two pages lists and create page , the default is "list"
  const [currentPage, setCurrentPage] = useState("list");
  // this will handle the list of birthdayList stored in array, the default is []
  const [birthdayList, setBirthdayList] = useState([]);
  // this will store the name of celebrant textinput value
  const [name, setName] = useState("");

  const [date, setDate] = useState(today);
  // this will validate the mode of picker either date picker and time picker
  const [mode, setMode] = useState("");
  // this will be the state for picker modal
  const [show, setShow] = useState(false);
  // this will be the value of date text inside the create page
  const [textDate, setTextDate] = useState(
    date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear()
  );
  // this will be the value of time text inside the create page
  const [textTime, setTextTime] = useState(
    date.getHours() + ":" + date.getMinutes()
  );

  // to use the push notification features we need to apply this variables
  const [expoPushToken, setExpoPushToken] = useState("");
  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();

  // to validate the selectiondate and get the seconds to be able to set into push notification
  const checkDate = () => {
    // intialize the new date so we can edit this value by not affecting the date state
    let tmp_bday = new Date(date);
    // this will console the birthday and today date
    console.log({ bdday: tmp_bday, today: today });
    // this will check the year of birth and today birthday
    if (tmp_bday.getFullYear() == today.getFullYear()) {
      // add 1 year if the statements is true
      tmp_bday.setFullYear(today.getFullYear() + 1);
    } else {
      // this is to create another temporary reference
      // to set the full year to currentyear of "tmp" temporary
      let tmp = new Date(tmp_bday);
      // try to set the year of tmp with current year
      tmp.setFullYear(today.getFullYear());
      // to validate tmp date if greater than today's  date
      //ex tmp = sep 26 2022  today = jun 15 2022
      if (tmp > today) {
        // set the current year
        tmp_bday.setFullYear(today.getFullYear());
      } else {
        // add 1 year because the tmp date is already passed
        tmp_bday.setFullYear(today.getFullYear() + 1);
      }
    }
    // this is to divide the milisecond by 1000
    // 1000 miliseconds is equivalent to 1 second
    let seconds = (tmp_bday.getTime() - today.getTime()) / 1000;
    // print the reference
    console.log({
      days: seconds / 86400,
      minutes: seconds / 60,
      seconds: seconds,
    });
    // return the value
    return seconds;
  };

  useEffect(() => {
    // to fetch the data that have been stored in async storage
    getData();
    // this is provided by expo notification from the documentation
    // expo notification setup
    registerForPushNotificationsAsync().then((token) =>
      setExpoPushToken(token)
    );

    notificationListener.current =
      Notifications.addNotificationReceivedListener((notification) => {
        setNotification(notification);
      });

    responseListener.current =
      Notifications.addNotificationResponseReceivedListener((response) => {
        console.log(response);
      });

    return () => {
      Notifications.removeNotificationSubscription(
        notificationListener.current
      );
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, []);

  // will fired if the time and date modal change
  const onChange = (event, selectedDate) => {
    // to see if the selectedDate is changed if not use the current date today
    const currentDate = selectedDate || date;
    setShow(Platform.OS === "ios");
    // set the state date
    setDate(currentDate);
    // format the selectedDate
    let tempDate = new Date(currentDate);
    // text format for date
    let fDate =
      tempDate.getMonth() +
      1 +
      "/" +
      tempDate.getDate() +
      "/" +
      tempDate.getFullYear();
    // text format for time
    let fTime = tempDate.getHours() + ":" + tempDate.getMinutes();
    setTextDate(fDate);
    setTextTime(fTime);
  };
  // clear the the stored data
  const clearDataHandler = () => {
    // set the AsyncStorage to value [] to reset the value of async storage
    storeData([]);
  };
  // add the data to storage
  const addHandler = async () => {
    if (name != "" && name.length > 2) {
      const secondDif = checkDate();
      // this will store the new data to async storage
      storeData([
        { name: name, date: textDate, time: textTime },
        ...birthdayList,
      ]);

      await schedulePushNotification(secondDif, name);
    }
  };
  // fetch the data from storage when this app launched
  const getData = async () => {
    try {
      // this will get the stored data from the application
      const birthdayStorage = await AsyncStorage.getItem("birthdayStorage");
      // check if the storage is null or not if not null then run the code below
      if (birthdayStorage !== null) {
        // should be parsed as json because birthdayStorage is not json type
        let stored = JSON.parse(birthdayStorage);
        // to update the state of birthdayList when component mount
        setBirthdayList(stored);
      }
    } catch (e) {
      console.log("Error occured: " + e.message);
    }
  };
  // store the data to storage
  const storeData = async (data) => {
    try {
      await AsyncStorage.setItem("birthdayStorage", JSON.stringify(data));
      // to update the state of birthdayList when component mount
      setBirthdayList(data);
      // to update the value of name state
      // setName("");
    } catch (e) {
      console.log("Error occured: " + e.message);
    }
  };
  // this function will calculate the remaining days and return the value
  const calculateDaysHandler = (dateString) => {
    // the format value of datestring is like 02/26/2021
    // split by / and this will return the value of array ["02","26","2021"]
    let temp_bday = dateString.split("/");
    today.setHours(0, 0, 0, 0);
    // to format date the upcomingBday variable
    // 2021/02/26
    let upcomingBday = new Date(
      today.getFullYear(),
      temp_bday[0] - 1,
      temp_bday[1]
    );
    // this will validate if the date is already passed to the current date
    if (today > upcomingBday) {
      // if this true set the year today by adding by 1
      upcomingBday.setFullYear(today.getFullYear() + 1);
    }

    // formula to get the day
    var one_day = 24 * 60 * 60 * 1000;
    // use Math.ceil to return a number not NaN error
    // get the upcomingBday and minus today time
    let daysLeft = Math.ceil(
      (upcomingBday.getTime() - today.getTime()) / one_day
    );
    // it will return the value
    return daysLeft;
  };

  return (
    <View style={styles.container}>
      {/* statusbar set to light mode  */}
      <StatusBar style="light" />
      {/* the header  */}
      <Text style={styles.header}>Birthday Reminder</Text>
      {/* button of list and create page UI */}
      <View style={{ width: "100%", flexDirection: "row" }}>
        {/* list button  */}
        <TouchableOpacity
          style={currentPage == "list" ? styles.pageBtn_active : styles.pageBtn}
          onPress={() => setCurrentPage("list")}
        >
          <View style={{ flexDirection: "row" }}>
            <Text
              style={[
                currentPage == "list"
                  ? styles.pageText_active
                  : styles.pageText,

                { marginRight: 5 },
              ]}
            >
              Lists
            </Text>
            <Text style={styles.badge}>{birthdayList.length}</Text>
          </View>
        </TouchableOpacity>
        {/* create button  */}
        <TouchableOpacity
          style={
            currentPage == "create" ? styles.pageBtn_active : styles.pageBtn
          }
          onPress={() => setCurrentPage("create")}
        >
          <Text
            style={
              currentPage == "create" ? styles.pageText_active : styles.pageText
            }
          >
            Create
          </Text>
        </TouchableOpacity>
      </View>
      {/* use ScrollView  */}
      <ScrollView
        style={{ height: "100%", backgroundColor: "white", width: "100%" }}
      >
        {/* display when currentPage is equal to create value  */}
        {currentPage == "create" ? (
          <>
            <View style={{ margin: 15 }}>
              <View>
                <Text style={styles.label}>Name:</Text>
                <TextInput
                  style={[
                    styles.field,
                    {
                      padding: 10,
                    },
                  ]}
                  placeholder="Enter name"
                  value={name}
                  onChangeText={(e) => setName(e)}
                />
              </View>
              <View>
                <Text style={styles.label}>Date of Birth:</Text>
                <TouchableOpacity
                  onPress={() => {
                    setShow(true);
                    setMode("date");
                  }}
                >
                  <Text style={styles.field}>{textDate}</Text>
                </TouchableOpacity>
              </View>
              <View>
                <Text style={styles.label}>Set Alarm:</Text>
                <TouchableOpacity
                  onPress={() => {
                    setShow(true);
                    setMode("time");
                  }}
                >
                  <Text style={styles.field}>{textTime}</Text>
                </TouchableOpacity>
              </View>

              <TouchableOpacity
                style={{ marginVertical: 20 }}
                title="Add to list"
                onPress={addHandler}
              >
                <Text style={styles.btn}>Add to Lists</Text>
              </TouchableOpacity>
            </View>
            {show && (
              <DateTimePicker
                testID="dateTimePicker"
                value={date}
                mode={mode}
                maximumDate={today}
                is24Hour={true}
                display="default"
                onChange={onChange}
              />
            )}
          </>
        ) : (
          <>
            {/* display when currentPage is equal to lists value  */}
            {/* show the birthdayList if the birthdayList length is greater thatn zero or else dont display  */}
            {birthdayList.length > 0 &&
              birthdayList.map((data, index) => (
                <View key={index} style={styles.card}>
                  <Text>Name: {data.name}</Text>
                  <Text>Birthdate: {data.date}</Text>
                  <Text>Alarm: {data.time}</Text>
                  <Text>Remaining Days: {calculateDaysHandler(data.date)}</Text>
                </View>
              ))}
            {/* if birthdayList have value by checking the length, show the clear data button  */}
            {birthdayList.length > 0 ? (
              <TouchableOpacity
                title="Clear data"
                style={{ margin: 15 }}
                onPress={() => clearDataHandler()}
              >
                <Text style={styles.btn}>Clear Data</Text>
              </TouchableOpacity>
            ) : (
              // else birthdayList have no stored data display the message
              <Text
                style={{
                  textAlign: "center",
                  paddingVertical: 40,
                  color: "#2a2e4b",
                }}
              >
                There is no birthday in the lists :(
              </Text>
            )}
          </>
        )}
      </ScrollView>
    </View>
  );
}

// copied from expo documentation
async function schedulePushNotification(seconds, name) {
  await Notifications.scheduleNotificationAsync({
    content: {
      title: "Birthday Reminder",
      body: "Hey, Today is " + name + "'s birthday!!!!",
      data: { data: "goes here" },
    },
    trigger: { seconds: seconds },
  });
}

async function registerForPushNotificationsAsync() {
  let token;
  if (Device.isDevice) {
    const { status: existingStatus } =
      await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    if (existingStatus !== "granted") {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }
    if (finalStatus !== "granted") {
      alert("Failed to get push token for push notification!");
      return;
    }
    token = (await Notifications.getExpoPushTokenAsync()).data;
    console.log(token);
  } else {
    alert("Must use physical device for Push Notifications");
  }

  if (Platform.OS === "android") {
    Notifications.setNotificationChannelAsync("default", {
      name: "default",
      importance: Notifications.AndroidImportance.MAX,
      vibrationPattern: [0, 250, 250, 250],
      lightColor: "#FF231F7C",
    });
  }

  return token;
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#2a2e4b",
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-start",
  },
  header: {
    marginTop: 24,
    fontSize: 25,
    width: "100%",
    textAlign: "center",
    paddingVertical: 15,
    fontWeight: "900",
    color: "#feffff",
    backgroundColor: "#2a2e4b",
  },
  badge: {
    backgroundColor: "#e74444",
    borderRadius: 20,
    textAlign: "center",
    color: "white",
    fontWeight: "bold",
    aspectRatio: 1 / 1,
    fontSize: 12,
    padding: 5,
  },
  pageBtn: {
    width: "50%",
    padding: 15,
    alignItems: "center",
    color: "white",
  },
  pageBtn_active: {
    width: "50%",
    padding: 15,
    alignItems: "center",
    backgroundColor: "#fff",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  pageText_active: {
    fontSize: 20,
    color: "#2a2e4b",
    fontWeight: "800",
    textAlign: "center",
  },
  pageText: {
    fontSize: 20,
    fontWeight: "800",
    textAlign: "center",
    color: "white",
  },
  card: {
    borderRadius: 15,
    marginTop: 15,
    marginHorizontal: 15,
    padding: 15,
    backgroundColor: "#f7f9ff",
  },
  btn: {
    fontSize: 15,
    textAlign: "center",
    padding: 15,
    color: "white",
    backgroundColor: "#e74444",
    borderRadius: 100,
  },
  field: {
    marginBottom: 5,
    padding: 15,
    paddingHorizontal: 25,
    borderColor: "#2a2e4b",
    borderWidth: 1,
    borderRadius: 105,
  },
  label: {
    padding: 10,
  },
});
